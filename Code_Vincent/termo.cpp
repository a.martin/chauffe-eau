#include "termo.h"




Termo::Termo()
{
  


}
float Termo::getTemp1()
{
  return getTemp(ThermistorPin1);
}
float Termo::getTemp2()
{
  return getTemp(ThermistorPin2);
}
float Termo::getTemp(int pin)
{
  float Vo;
  float R1 = 10000;
  float logR2, R2, T, Tc;
  float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741;
  
  Vo = analogRead(pin);
  R2 = R1 * (1023.0 / Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15;
  Serial.print("Temperature: "); 
  Serial.print(Tc);
  Serial.println(" C");   
  /*Serial.println(Vo);*/
  char transchar;
  transchar = char(Tc);

  delay(1000);
  return 0;
  
}
