#ifndef termo_h
#define termo_h
#include "Arduino.h"
#define ThermistorPin1  A0
#define ThermistorPin2  32

class Termo
{
  public:
  Termo();
  float getTemp1();
  float getTemp2();
  private:
  float getTemp(int);
  
};
#endif
