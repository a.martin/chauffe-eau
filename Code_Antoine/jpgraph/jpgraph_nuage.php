<?php //content="text/plain; charset=utf-8"
require_once ('src/jpgraph.php');
require_once ('src/jpgraph_scatter.php');
require_once ('src/jpgraph_date.php');
require_once ('src/jpgraph_line.php');
 
require_once '../inc/connect.php';

$connect = connectBdd_PDO();

$sql = "SELECT valeur_mesure, heure_mesure FROM t_mesure WHERE id_capteur = -1 ORDER BY id_mesure DESC LIMIT 20";
//$donnees = array(23,9,58,23,26,57,48,12);
//$datay = array();
//$datax = array();
//DEFINE('NDATAPOINTS',360);
//DEFINE('SAMPLERATE',240); 
$datay = array();
$datax = array();
foreach ($connect->query($sql) as $row)
{
    //$machin = new DateTime ($row['heure_mesure']);
//    array_push($datay, $row['valeur_mesure']);
//    array_push($datax, $machin);
    //array_push($datax, idate('B', $row['heure_mesure']);
    
    //var_dump($row['heure_mesure']);
    //$trucmuche = idate($tuile, $timestamp = time());
    //var_dump ($trucmuche);

$start = $row['heure_mesure'] ;
//$end = $start+NDATAPOINTS*SAMPLERATE;
array_push($datay, $row['valeur_mesure']);
// $datax = $row['heure_mesure'];
//array_push($datax, $row['heure_mesure']);
//var_dump($row['heure_mesure']);
//var_dump(time('Y-m-d H:i:s', $row['heure_mesure']));
//array_push($datax, time('Y-m-d H:i:s', $row['heure_mesure']));
$date = new DateTime ($row['heure_mesure']);
/*echo time('Y-m-d H:i:s', $row['heure_mesure']) . "<br>";
echo $row['heure_mesure'] . "<br>";
var_dump($date) ; echo "<br>";
echo $date->getTimestamp() . "<br>";
 */
//array_push($datax, strtotime(date('Y-m-d H:i:s', $row['heure_mesure'])));
array_push($datax, $date->getTimestamp());

}

//var_dump()



/*
// Create a data set in range (50,70) and X-positions
DEFINE('NDATAPOINTS',10);
DEFINE('SAMPLERATE',240); 
$start = time();
$end = $start+NDATAPOINTS*SAMPLERATE;
$datay = array();
$datax = array();
for( $i=0; $i < NDATAPOINTS; ++$i ) {
    $datay[$i] = rand(50,70);
    $datax[$i] = $start + $i * SAMPLERATE;
}*/

//$datax = array(3.5,3.7,3,4,6.2,6,3.5,8,14,8,11.1,13.7);
//$datay = array(100,22,12,13,17,20,16,19,30,31,40,43);


 
$graph = new Graph(600,600);
$graph->SetScale('datlin');
$graph->xaxis->scale->SetDateFormat('d/m');
//$graph->SetScale("linlin");
 
$graph->xaxis->SetTextTickInterval(30,0);// le nombre des éléments de chaque tanche  
/*
 à ce niveau on a un ensemble d'éléments, formé par le premier  élément de chaque tranche
 1 signifie qu'on affiche tous les éléments, 2 signifie qu'on affiche un élément et on saut un donc on affiche la moitie des élément par ordre 
*/
$graph->xaxis->SetTextLabelInterval(2);


$graph->img->SetMargin(40,40,40,40);        
$graph->SetShadow();
 
$graph->title->Set("Capteur temperature : sortie panneau solaire");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
 
$sp1 = new ScatterPlot($datay,$datax);
 
$graph->Add($sp1);
$graph->Stroke();


 
?>

