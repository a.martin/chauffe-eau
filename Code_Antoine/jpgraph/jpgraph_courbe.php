<?php
require_once("include_path_inc.php");

require_once("src/jpgraph.php");
require_once("src/jpgraph_line.php");
require_once '../inc/connect.php';

$connect = connectBdd_PDO();

$sql = "SELECT valeur_mesure FROM t_mesure WHERE id_capteur = -1";
//$donnees = array(23,9,58,23,26,57,48,12);
$donnees = array();
foreach ($connect->query($sql) as $row)
{
    array_push($donnees, $row['valeur_mesure']);
    
}







$largeur = 500;
$hauteur = 500;

// Initialisation du graphique
$graphe = new Graph($largeur, $hauteur);
// Echelle lineaire ('lin') en ordonnee et pas de valeur en abscisse ('text')
// Valeurs min et max seront determinees automatiquement
$graphe->setScale("textlin");

// Creation de la courbe
$courbe = new LinePlot($donnees);
// Ajout de la courbe au graphique
$graphe->add($courbe);

// Ajout du titre du graphique
$graphe->title->set("Courbe");

// Affichage du graphique
$graphe->stroke();
?>