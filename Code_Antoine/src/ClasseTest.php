<?php

require_once 'Mesures.php';
require_once 'Meteo.php';
require_once 'RepositoryDernieresMesures.php';
require_once 'RepositoryConsommationResistance.php';


// teste d'insertion de mesures
$maMesures = new EntityMesures(1000, time());
$repo = new RepositoryMesures();


//insertion mesure du capteur de température à la sortie du panneau
$res = $repo->InsertionMesuresCapteurTemperatureSortiePanneau($maMesures);

if($res >= 1)
{
    echo "Insertion des mesures des capteurs : ";
    echo " Le capteur de tempétature à la sortie du panneau, a inséré la valeur " . $maMesures->_valeur . " °C dans la BDD, à la date " . date('Y-m-d H:i:s',$maMesures->_dateHeure) . "<br>";
}
 else 
{
    echo "Insertion des mesures des capteurs : ";
    echo 'ECHEC Insertion ';
}

//insertion mesure du capteur de débit côté panneau
$res2 = $repo->InsertionMesuresCapteurDebitPanneau($maMesures);
if($res2 >=1)
{
    echo "Insertion des mesures des capteurs : ";
    echo " Le capteur de debit à la sortie du panneaut a inséré la valeur " . $maMesures->_valeur . " m^3/sec dans la BDD, à la date " . date('Y-m-d H:i:s',$maMesures->_dateHeure). "<br>" ."<br>";
}
 else {
    echo "Insertion des mesures des capteurs : ";
    echo 'ECHEC Insertion';
}
// teste insertion donnée météo
$maMeteo = new EntityMeteo(23.3, "Soleil", "2019-03-27 00:00:00", "2019-03-27 03:00:00", 10);
$repo2 = new RepositoryMeteo();

$res = $repo2->InsertionValeurMeteo($maMeteo);
if ($res >= 1)
{
    echo 'Insertion données météo : ';
    echo 'Les prévision météo on été inséré dans la bdd. Elle annonce une température de ' . $maMeteo->_temperature . ' °C, avec un id de temps ' . $maMeteo->_typeTemps . ' entre ' . $maMeteo->_heureDebut . ' et ' . $maMeteo->_heureFin . '. Le taux de nuage est de ' . $maMeteo->_pourcentageNuage."<br>" . "<br>" ;

    echo 'Connaitre id du type de temps : le type de temps à pour id :' . $repo2->getidTypeTemps('Test'). "<br>" . "<br>"; 
}

 else 
{
     echo 'Insertion données météo : ';
    echo 'Echec insertion';
}


//dernière valeur
$maMesure = $repo->getDerniereMesures(-1);
echo 'Dernière mesure du capteur -1 : ' . $maMesure->_valeur.'<br>';
echo $maMesure->_dateHeure.'<br>';

$maMesure = $repo->getDerniereMesures(-9);
echo'Dernièere mesure du capteur -9 : ';
echo $maMesure->_valeur?$maMesure->_valeur:'null'.'<br>';
echo '</br>';
echo $maMesure->_dateHeure.'<br>'.'<br>';



//consommation résistance
$repo3 = new RepositoryConsommationResistance();
$maConsoResitance = $repo3->ResistanceEnMarche();
echo "maConsoResitance = " . ($maConsoResitance?"VRAI":"FAUX").'<br>';


$repo4 = new RepositoryDernieresMesures();
$mesMesures = $repo4->RecupererDernieresMesures();
echo $mesMesures->resistanceEnMarche;