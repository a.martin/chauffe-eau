<?php

require_once '../inc/connect.php';

class EntityMeteo 
{
    public $_temperature;
    public $_typeTemps;
    public $_heureDebut;
    public $_heureFin;
    public $_pourcentageNuage;
    
            
    function __construct($temperature, $typeTemps, $heureDebut, $heureFin, $pourcentageNuage) 
    {
        $this->_temperature = $temperature;
        $this->_typeTemps = $typeTemps;
        $this->_heureDebut = $heureDebut;
        $this->_heureFin = $heureFin;
        $this->_pourcentageNuage = $pourcentageNuage;
        
    }
}


class RepositoryMeteo
{
    public function getidTypeTemps($nomTypeTemps)
    {
         $connect = connectBdd_PDO();
         
          $sql = "SELECT id_type_temps FROM t_type_temps WHERE nom_temps='$nomTypeTemps';";
           
               $res = $connect->query($sql);
               $row = $res->fetch(PDO::FETCH_ASSOC); 

               
               
               if ($row)
               {
                   
                   //il existe déjà un type de temps dans la bdd
                   //il faut recupérer la valeur $res et le renvoyer
                   //$row = $tab[0];
                   $id = $row['id_type_temps'];
                   
//                   var_dump($res);
//                   var_dump($row);
//                   var_dump($id);
                   return $id;
                   
               }
               else // se type de temps n'existe pas encore, il faut donc l'insérer dans la bdd
               {
                   
                   $sql = "INSERT INTO t_type_temps(nom_temps) VALUES ('$nomTypeTemps');";
                   $res = $connect->exec($sql);
                   if ($res)
                   {
                       //echo 'Insertion OK' . '<br>';
                       
                       $sql = "SELECT id_type_temps FROM t_type_temps WHERE nom_temps='$nomTypeTemps';";
                       $res = $connect->query($sql);
                       $row = $res->fetch(PDO::FETCH_ASSOC);
                       $id = $row['id_type_temps'];
                       return $id;
                   }
                   
                   else
                   {
                       return "Echec de l'insertion" . '<br>';
                   }
               }  
    } 
    
  

    public function InsertionValeurMeteo(EntityMeteo $prevision)
    {
        $connect = connectBdd_PDO();
        
        
        $typeTemps = $this->getidTypeTemps($prevision->_typeTemps);
        
        
        $sql = "INSERT INTO t_previsionmeteo_heure (temperature_horaire, id_nom_temps, dateheure_debut, dateheure_fin, pourcentage_nuage) VALUES ($prevision->_temperature, $typeTemps, '$prevision->_heureDebut', '$prevision->_heureFin', $prevision->_pourcentageNuage)";
      
        $res = $connect->exec($sql);
//        if($res)
//            {
//                echo 'Insertion OK.';
//            }
//        else
//            {
//                $errorInfo = $connect->errorInfo() ;
//                echo 'ECHEC Insertion : '.$errorInfo[2] .'<br>' ;
//            }
       /* $res->closeCursor(); // this is not even required
        $res = null; // doing this is mandatory for connection to get closed
        $connect = null;*/
        return $res;
    }
}
