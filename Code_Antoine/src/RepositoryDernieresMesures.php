<?php

require_once 'EntityDernieresMesures.php';
require_once '../inc/connect.php';
require_once 'RepositoryConsommationResistance.php';
require_once 'Mesures.php';

class RepositoryDernieresMesures 
{
    public function RecupererDernieresMesures()
    {
                $connect = connectBdd_PDO();

                $date = date('Y-m-d H:i:s', time());
                
        $dernieresMesures = new EntityDernieresMesures();
        $repo = new RepositoryMesures();
         
        $maMesure = $repo->getDerniereMesures(-6);
        $dernieresMesures->CapteurAccumulateur1_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur1_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-7);
        $dernieresMesures->CapteurAccumulateur2_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur2_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-8);
        $dernieresMesures->CapteurAccumulateur3_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur3_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-2);
        $dernieresMesures->CapteurDébitPanneau_heure = $maMesure->_dateHeure;        
        $dernieresMesures->CapteurDébitPanneau_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-17);
        $dernieresMesures->CapteurDébitDouche_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurDébitDouche_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-1);
        $dernieresMesures->CapteurSortiePanneau_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurSortiePanneau_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-3);
        $dernieresMesures->CapteurEntréeAccumulateurCôtéPanneau_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurEntréeAccumulateurCôtéPanneau_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-4);
        $dernieresMesures->CapteurEntréePanneau_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurEntréePanneau_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-5);
        $dernieresMesures->CapteurSortieAccumulateurCôtéPanneau_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurSortieAccumulateurCôtéPanneau_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-16);
        $dernieresMesures->CapteurSortieAccumulateurHautCôtéDouche_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurSortieAccumulateurHautCôtéDouche_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-18);
        $dernieresMesures->CapteurEntréeDouche_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurEntréeDouche_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-9);
        $dernieresMesures->CapteurAccumulateur4_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur4_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-10);
        $dernieresMesures->CapteurAccumulateur5_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur5_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-11);
        $dernieresMesures->CapteurAccumulateur6_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur6_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-12);
        $dernieresMesures->CapteurAccumulateur7_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur7_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-13);
        $dernieresMesures->CapteurAccumulateur8_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur8_valeur = $maMesure->_valeur;
        
        $maMesure = $repo->getDerniereMesures(-14);
        $dernieresMesures->CapteurAccumulateur9_heure = $maMesure->_dateHeure;
        $dernieresMesures->CapteurAccumulateur9_valeur = $maMesure->_valeur;

        $repo3 = new RepositoryConsommationResistance();
        $maConsoResistance = $repo3->ResistanceEnMarche();
        $dernieresMesures->resistanceEnMarche = $maConsoResistance;
        
        return $dernieresMesures;
    }
    
   
    
}
