<?php

require_once '../inc/connect.php';

class EntityMesures
{
    public $_valeur;
    public $_dateHeure;
             

    function __construct($valeur, $dateHeure)
    {
        $this->_valeur = $valeur;
        $this->_dateHeure = $dateHeure;
    }

}

class RepositoryMesures
{
    public function InsertionMesuresCapteurTemperatureSortiePanneau($mesure)
    {
        return $this->InsertionMesuresCapteur($mesure, -1);
    }
    
    public function InsertionMesuresCapteurDebitPanneau($mesure)
    {
       return $this->InsertionMesuresCapteur($mesure, -2);
    }
    
    public function InsertionMesuresCapteurTemperatureEntreeAccumulateur($mesure)
    {
       return $this->InsertionMesuresCapteur($mesure, -3);
    }
    
    public function InsertionMesuresCapteurTemperatureEntreePanneau($mesure)
    {
       return $this->InsertionMesuresCapteur($mesure, -4);
    }
    
    public function InsertionMesuresCapteurTemperatureSortieAccumulateurBas($mesure)
    {
       return $this->InsertionMesuresCapteur($mesure, -5);
    }
    
    public function InsertionMesuresCapteurTemperature1DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -6);
    }
    
    public function InsertionMesuresCapteurTemperature2DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -7);
    }
    
    public function InsertionMesuresCapteurTemperature3DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -8);
    }
    
    public function InsertionMesuresCapteurTemperature4DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -9);
    }
    
    public function InsertionMesuresCapteurTemperature5DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -10);
    }
    
    public function InsertionMesuresCapteurTemperature6DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -11);
    }
    
    public function InsertionMesuresCapteurTemperature7DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -12);
    }
    
    public function InsertionMesuresCapteurTemperature8DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -13);
    }
    
    public function InsertionMesuresCapteurTemperature9DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -14);
    }
    
    public function InsertionMesuresCapteurTemperature10DansAccumulateur($mesure) //en partant du bas
    {
       return $this->InsertionMesuresCapteur($mesure, -15);
    }
    
    public function InsertionMesuresCapteurTemperatureSortieAccumulateurHaut($mesure) 
    {
       return $this->InsertionMesuresCapteur($mesure, -16);
    }
    
    public function InsertionMesuresCapteurDebitDouche($mesure) 
    {
       return $this->InsertionMesuresCapteur($mesure, -17);
    }
    
    public function InsertionMesuresCapteurTemperatureEntreeDouche($mesure) 
    {
        return $this->InsertionMesuresCapteur($mesure, -18);
    }
    
//    public function InsertionMesuresCapteurLumiereStationMeteo($mesure) 
//    {
//        $this->InsertionMesuresCapteur($mesure, -19);
//    }
//    
//    public function InsertionMesuresCapteurHygrometrieStationMeteo($mesure) 
//    {
//        $this->InsertionMesuresCapteur($mesure, -20);
//    }
    
    
    
    
    
    public function InsertionMesuresCapteur($mesure, $idCapteur)
    {
        $connect = connectBdd_PDO();
        

        $date = date('Y-m-d H:i:s', $mesure->_dateHeure);
        
        
        $sql = "INSERT INTO t_mesure (valeur_mesure, heure_mesure, id_capteur) VALUES ($mesure->_valeur, '$date', $idCapteur)";
      
        $res = $connect->exec($sql);
        
//        if($res)
//            {
//                echo 'Insertion OK.';
//            }
//        else
//            {
//                $errorInfo = $connect->errorInfo() ;
//                echo 'ECHEC Insertion : '.$errorInfo[2] .'<br>' ;
//            }
       
         
        
        /*$res->closeCursor(); // this is not even required
        $res = null; // doing this is mandatory for connection to get closed
        $connect = null;*/
        
        return $res;
       
          
    }
    
    
   public function getDerniereMesures($idCapteur)
   {
       $connect = connectBdd_PDO();
       
       $sql = "SELECT* FROM t_mesure WHERE id_capteur = " . $idCapteur . "ORDER BY heure_mesure DESC LIMIT 1";
       
       $res = $connect->query($sql);
       $row = $res->fetch(PDO::FETCH_ASSOC); 
       
       
       
       $maMesure = new EntityMesures($row['valeur_mesure'], $row['heure_mesure']);
       
       return $maMesure;
   }
    
    

}


       

