<?php


require_once '../inc/connect.php';

class RepositoryConsommationResistance
{
    public function ResistanceEnMarche()
    {
        $connect = connectBdd_PDO();
        
        $sql = "SELECT COUNT(*) AS nb FROM t_consommationresistance WHERE heure_fin_conso IS NULL ";
        
        $res = $connect->query($sql);
        $row = $res->fetch(PDO::FETCH_ASSOC);
        //var_dump($row);
        $nb = $row['nb'];
//        var_dump($nb) ;
        if($nb == 0)
        {
            return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
}
