<?php


class EntityDernieresMesures 
{
    public $CapteurSortiePanneau_valeur;
    public $CapteurSortiePanneau_heure;
    public $CapteurDébitPanneau_valeur;
    public $CapteurDébitPanneau_heure;
    public $CapteurEntréeAccumulateurCôtéPanneau_valeur;
    public $CapteurEntréeAccumulateurCôtéPanneau_heure;
    public $CapteurEntréePanneau_valeur;
    public $CapteurEntréePanneau_heure;
    public $CapteurSortieAccumulateurCôtéPanneau_valeur;
    public $CapteurSortieAccumulateurCôtéPanneau_heure;
    public $CapteurAccumulateur1_valeur;
    public $CapteurAccumulateur1_heure;
    public $CapteurAccumulateur2_valeur;
    public $CapteurAccumulateur2_heure;
    public $CapteurAccumulateur3_valeur;
    public $CapteurAccumulateur3_heure;
    public $CapteurAccumulateur4_valeur;
    public $CapteurAccumulateur4_heure;
    public $CapteurAccumulateur5_valeur;
    public $CapteurAccumulateur5_heure;
    public $CapteurAccumulateur6_valeur;
    public $CapteurAccumulateur6_heure;
    public $CapteurAccumulateur7_valeur;
    public $CapteurAccumulateur7_heure;
    public $CapteurAccumulateur8_valeur;
    public $CapteurAccumulateur8_heure;
    public $CapteurAccumulateur9_valeur;
    public $CapteurAccumulateur9_heure;
    public $CapteurSortieAccumulateurHautCôtéDouche_valeur;
    public $CapteurSortieAccumulateurHautCôtéDouche_heure;
    public $CapteurDébitDouche_valeur;
    public $CapteurDébitDouche_heure;
    public $CapteurEntréeDouche_valeur;
    public $CapteurEntréeDouche_heure;
    public $CapteurLumièreStationMeteo_valeur;
    public $CapteurLumièreStationMeteo_heure;
    public $CapteurHygrometrieStationMeteo_valeur;
    public $CapteurHygrometrieStationMeteo_heure;
    public $resistanceEnMarche;
}

