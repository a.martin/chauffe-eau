<?php

require_once '../inc/connect.php';

session_start();
$connect = connectBdd_PDO();


if (isset($_POST["BoutonConnexion"]))
{
    if (!empty($_POST["email"]) AND !empty($_POST["motDePasse"]))
    {
        $email = htmlspecialchars($_POST["email"]);
        $mdp = htmlspecialchars($_POST["motDePasse"]);
        $hashmdp = sha1($mdp);
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $verifmail = "SELECT mail_utilisateur FROM t_utilisateur WHERE mail_utilisateur = '".$email."'";
            $res = $connect->query($verifmail);
            //var_dump($verifmail);
            if ($res->rowCount() == 1)
            {
                $verifmdp = "SELECT prenom_utilisateur, nom_utilisateur FROM t_utilisateur WHERE mdp_utilisateur = '".$hashmdp."' AND mail_utilisateur = '".$email."'";
                //echo $verifmdp
                $res = $connect->query($verifmdp);
                //var_dump($verifmdp);
                if ($res->rowCount() == 1)
                {
                    $row = $res->fetch(PDO::FETCH_ASSOC); 
                    $_SESSION["nom_utilisateur"] = $row['nom_utilisateur'];
                    $_SESSION["prenom_utilisateur"] = $row['prenom_utilisateur'];
                    
                    //echo $row['nom_utilisateur'];
                    //echo $err;
                    
                    header("Location: index.php");
                }
                else
                {
                    echo 'Mauvais mot de passe ou identifiant';
                }
            }
            else
            {
                echo 'Adresse mail non reconnu';
            }
        }
        else
        {
            echo 'Adresse mail non valide';
        }
    }
    else
    {
        echo 'Vous devez tout remplir';
    }
}



