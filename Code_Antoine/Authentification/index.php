    <!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

    <?php session_start(); ?>
<html lang="fr">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Chauffe Eau</title>
      <link rel="stylesheet" href="style.css">
        <script>function monAjax() {
				var loReq = new XMLHttpRequest();
				loReq.open("GET", "accueil.php?offsetY=124", false);
				loReq.send(null);
				if (loReq.status === 200) {
					document.getElementById("maDiv").innerHTML = loReq.responseText
				} else {
					document.getElementById("maDiv").innerHTML = "Erreur" ;
				}
			}
                        window.onload = function() {
                            setInterval(monAjax, 10000);
                            monAjax();
                        }

        </script>
    </head>
    <body>
           
  <nav class="navbar">
    <span class="open-slide">
      <a href="#" onclick="openSlideMenu()">
        <svg width="30" height="30">
            <path d="M0,5 30,5" stroke="#fff" stroke-width="5"/>
            <path d="M0,14 30,14" stroke="#fff" stroke-width="5"/>
            <path d="M0,23 30,23" stroke="#fff" stroke-width="5"/>
        </svg>
      </a>
        <div style = "color:white">
            <br/>
        <?php 
        if(isset($_SESSION["prenom_utilisateur"])&& isset($_SESSION["nom_utilisateur"])) 
        {
            //echo"<h1>La connexion est réussie!!!!</h1>";
            echo "Bienvenue, " . $_SESSION["prenom_utilisateur"]. " " . $_SESSION["nom_utilisateur"]; 
        }
        else 
        {
             header("Location: ../WebsiteMenu/Menu.html");
        }   
        ?>            
        
        <div style="float:right; color:white;margin-right: 160px;">Projet Chauffe-Eau</div></div>
    </span>


  </nav>

  <div id="side-menu" class="side-nav">
    <a href="#" class="btn-close" onclick="closeSlideMenu()">&times;</a>
    <a href="#">Météo</a>
    <a href="#">Températures</a>
    <a href="#">Débits</a>
    <a href="#">Ma Consommation</a>
    <a href="../jpgraph/jpgraph_nuage.php">Historique</a>
    <a href="#">Paramètres</a>
    <a href="#">Contact</a>
    
    <a href="indexConnexion.html">Déconnexion <?php session_unset(); session_destroy();?></a> 
    
  </div>

  <div id="main">
    <h1></h1>
  </div>

  <script>
    function openSlideMenu(){
      document.getElementById('side-menu').style.width = '250px';
      document.getElementById('main').style.marginLeft = '250px';
    }

    function closeSlideMenu(){
      document.getElementById('side-menu').style.width = '0';
      document.getElementById('main').style.marginLeft = '0';
    }
  </script>
        
        <div id ="maDiv"></div>
        
       
        
                
        
    </body>
</html>
