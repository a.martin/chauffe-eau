

CREATE TABLE t_utilisateur (
	id_utilisateur SERIAL NOT NULL,
	nom_utilisateur VARCHAR(10) NOT NULL,
	prenom_utilisateur VARCHAR(10) NOT NULL,
	adresse_utilisateur VARCHAR(50),
	ville_utilisateur VARCHAR(50),
	tel_utilisateur INT,
	mail_utilisateur VARCHAR(50) NOT NULL,
	mdp_utilisateur VARCHAR(50) NOT NULL,
	CONSTRAINT t_utilisateur_PK PRIMARY KEY(id_utilisateur)
) ;


CREATE TABLE t_type_capteur (
	id_type_capteur SERIAL NOT NULL,
	nom_type_capteur VARCHAR(50),
	unite_type_capteur VARCHAR(10),
	CONSTRAINT t_type_capteur_PK PRIMARY KEY (id_type_capteur)
);


CREATE TABLE t_capteur (
	id_capteur SERIAL NOT NULL,
	id_type_capteur INT NOT NULL,
	nom_capteur VARCHAR(50),
	description_capteur TEXT,
	CONSTRAINT t_capteur_PK PRIMARY KEY (id_capteur),
	CONSTRAINT t_capteur_t_type_capteur_FK FOREIGN KEY (id_type_capteur) REFERENCES t_type_capteur(id_type_capteur)
);


CREATE TABLE t_mesure (
	id_mesure SERIAL NOT NULL,
	valeur_mesure FLOAT,
	heure_mesure TIMESTAMP,
	id_capteur INT NOT NULL,
	CONSTRAINT t_mesure_PK PRIMARY KEY (id_mesure),
	CONSTRAINT t_mesure_t_capteur_FK FOREIGN KEY (id_capteur) REFERENCES t_capteur(id_capteur)
);


CREATE TABLE t_consommationresistance (
	id_conso SERIAL NOT NULL,
	heure_debut_conso TIMESTAMP,
	heure_fin_conso TIMESTAMP,
	CONSTRAINT t_consommationresistance_PK PRIMARY KEY (id_conso)
);


CREATE TABLE t_type_temps (
	id_type_temps SERIAL NOT NULL,
	nom_temps VARCHAR(50),
	CONSTRAINT t_type_temps_PK PRIMARY KEY (id_type_temps)
);



CREATE TABLE t_previsionmeteo_heure (
	id_prevhoraire SERIAL NOT NULL,
	temperature_horaire FLOAT,
	pourcentage_nuage INT,
	id_nom_temps INT,
	dateHeure_debut TIMESTAMP,
	dateHeure_fin TIMESTAMP,
	CONSTRAINT t_previsionmeteo_heure_PK PRIMARY KEY (id_prevhoraire),
	CONSTRAINT t_previsionmeteo_heure_t_type_temps_FK FOREIGN KEY (id_nom_temps) REFERENCES t_type_temps(id_type_temps)
);

CREATE TABLE t_meteo_reelle (
	id_meteo SERIAL NOT NULL,
	temperature_horaire FLOAT,
	pourcentage_nuage INT,
	id_nom_temps INT,
	dateHeure TIMESTAMP,
	CONSTRAINT t_meteo_reelle_PK PRIMARY KEY (id_meteo),
	CONSTRAINT t_meteo_reelle_t_type_temps_FK FOREIGN KEY (id_nom_temps) REFERENCES t_type_temps(id_type_temps)
	

);


CREATE TABLE t_log_type (
	id_log_type SERIAL NOT NULL,
	libelle_type VARCHAR(10),
	CONSTRAINT t_log_type_PK PRIMARY KEY (id_log_type)
);


CREATE TABLE t_log (
	id_log SERIAL NOT NULL,
	table_origine VARCHAR(50),
	identite_utilisateur varchar(50),
	date DATE,
	id_log_type INT,
	CONSTRAINT t_log_PK PRIMARY KEY (id_log),
	CONSTRAINT t_log_t_log_type_FK FOREIGN KEY (id_log_type) REFERENCES t_log_type (id_log_type)
);
	