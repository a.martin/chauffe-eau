INSERT INTO t_utilisateur (nom_utilisateur, prenom_utilisateur, adresse_utilisateur, ville_utilisateur, tel_utilisateur, mail_utilisateur, mdp_utilisateur) 
	VALUES ('Martin', 'Antoine', '12 rue abebrtiun', 'Lyon', 0672626387, 'a.martin@gmail.com', '12345'),
		   ('Givaudan', 'Matthieux', '13 rue abebrtiun', 'Lyon', 0672737238, 'm.givaudan@gmail.com', '54321'),
		   ('Valeyre', 'Sioban', '14 rue abebrtiun', 'Lyon', 0629381838, 's.valeyre@gmail.com', '34251'),
		   ('Rodriguez', 'Luca', '15 rue abebrtiun', 'Lyon', 0629382983, 'l.rodriguez@gmail.com', '12453'),
		   ('Aidoud', 'Abde', '15 rue abebrtiun', 'Lyon', 0683672736, 'a.aidoud@gmail.com', '45312'),
		   ('Peirano', 'Vincent', '16 rue abebrtiun', 'Lyon', 0635183247, 'v.peirano@gmail.com', '15243');
		   


INSERT INTO t_type_capteur(id_type_capteur, nom_type_capteur, unite_type_capteur) 
	VALUES (-1, 'capteur de debit', 'm3/sec'),
		   (-2, 'capteur de lumiere', 'lumen'),
		   (-3, 'capteur hygrometrie', 'en %'),
		   (-4, 'capteur de temperature analogique', '�C'),
		   (-5, 'capteur de temperature numerique', '�C');


INSERT INTO t_capteur(id_capteur, nom_capteur, description_capteur, id_type_capteur) 
	VALUES (-1, 'capteur sortie panneau', 'capteur de temp�rature analogique en sortie du panneau solaire', -4),
		   (-2, 'capteur d�bit panneau', 'capteur de d�bit � proximit� du panneau solaire', -1),
		   (-3, 'capteur entr�e accumulateur c�t� panneau', 'capteur de temp�rature analogique en entr�e accumulateur', -4),
		   (-4, 'capteur entr�e panneau', 'capteur de temp�rature analogique en entr�e du panneau solaire', -4),
		   (-5, 'capteur sortie accumulateur c�t� panneau', 'capteur de temp�rature analogique en sortie de accumulateur', -4),
		   (-6, 'capteur accumulateur 1', 'capteur de temperature num�rique tout en bas de accumulateur', -5),
		   (-7, 'capteur accumulateur 2', 'capteur de temperature num�rique 2�me en partant du bas de accumulateur', -5),
		   (-8, 'capteur accumulateur 3', 'capteur de temperature num�rique 3�me en partant du bas de accumulateur', -5),
		   (-9, 'capteur accumulateur 4', 'capteur de temperature num�rique 4�me en partant du bas de accumulateur', -5),
		   (-10, 'capteur accumulateur 5', 'capteur de temperature num�rique 5�me en partant du bas de accumulateur', -5),
		   (-11, 'capteur accumulateur 6', 'capteur de temperature num�rique 6�me en partant du bas de accumulateur', -5),
		   (-12, 'capteur accumulateur 7', 'capteur de temperature num�rique 7�me en partant du bas de accumulateur', -5),
		   (-13, 'capteur accumulateur 8', 'capteur de temperature num�rique 8�me en partant du bas de accumulateur', -5),
		   (-14, 'capteur accumulateur 9', 'capteur de temperature num�rique 9�me en partant du bas de accumulateur', -5),
		   
		   (-16, 'capteur sortie accumulateur haut c�t� douche', 'capteur de temperature analogique en sortie du haut de accumulateur', -4),
		   (-17, 'capteur d�bit douche', 'capteur de debit � proximit� de la douche', -1),
		   (-18, 'capteur entr�e douche', 'capteur de temperature analogique en entr�e de la douche', -4),
		   (-19, 'capteur lumi�re station meteo', 'capteur de lumiere dans la station meteo', -2),
		   (-20, 'capteur  hygrometrie station meteo','capteur hygrometrie dans la station meteo', -3);


INSERT INTO t_mesure(valeur_mesure, heure_mesure, id_capteur) 
	VALUES     (3.14, '2019-02-08 10:12:14', -2),  
		   (78.53, '2019-02-07  15:07:28', -3),
		   (56.23, '2019-02-07 13:52:09', -1),
		   (35.45, '2019-02-07 19:19:47', -5),
		   (18.07, '2019-02-06 09:45:07', -4),
                   (10.01, '2019-02-06 07:09:10', -6),
                   (11.02, '2019-02-06 07:10:10', -7),
                   (12.03, '2019-02-06 07:11:10', -8),
                   (13.04, '2019-02-06 07:12:10', -9),
                   (14.05, '2019-02-06 07:13:10', -10),
                   (15.02, '2019-02-06 07:10:10', -11),
                   (16.02, '2019-02-06 07:10:10', -12),
                   (17.02, '2019-02-06 07:10:10', -13),
                   (18.02, '2019-02-06 07:10:10', -14),
                   (19.02, '2019-02-06 07:10:10', -16),
                   (18.02, '2019-02-06 07:10:10', -17),
                   (11.03, '2019-02-06 07:10:10', -18);
INSERT INTO t_mesure(valeur_mesure, heure_mesure, id_capteur) 
	VALUES     
		   (35.45, '2019-02-08 19:19:47', -5),
(36.45, '2019-02-09 19:19:47', -5),
(37.45, '2019-02-10 19:19:47', -5),
(38.45, '2019-02-11 19:19:47', -5);
		   
                   


INSERT INTO t_consommationresistance(heure_debut_conso, heure_fin_conso) 
	VALUES ('2019-02-08 13:58:42', '2019-02-08 14:12:09'),
		   ('2019-02-07 04:35:16', '2019-02-07 08:00:01');
		   
		   
INSERT INTO t_type_temps (id_type_temps, nom_temps)
	VALUES (-1, 'Ensoleille'),
		   (-2, 'Nuageux'),
		   (-3, 'Pluie'),
		   (-4, 'pluie legere'),
		   (-5, 'Averse de pluie'),
		   (-6, 'Partiellement ensoleille'),
		   (-7, 'Globalement nuageux'),
		   (-8, 'neige');
		   



		   
		   
INSERT INTO t_previsionmeteo_heure(temperature_horaire, id_nom_temps, pourcentage_nuage, dateheure_debut, dateheure_fin)	
	VALUES (3, -3, 10, '2019-02-01 12:00:00', '2019-02-01 15:00:00'),
		   (5, -3, 27, '2019-02-01 15:00:00', '2019-02-01 18:00:00'),
		   (6, -2, 57, '2019-02-01 18:00:00', '2019-02-01 21:00:00'),
		   (7, -2, 86, '2019-02-01 21:00:00', '2019-02-01 00:00:00');
		   
		   
		   
INSERT INTO t_meteo_reelle (temperature_horaire, pourcentage_nuage, id_nom_temps, dateHeure)
	VALUES (4, 8, -3, '2019-02-08 08:02:00');
           
		   
		   
	
INSERT INTO t_log_type(id_log_type, libelle_type) 
	VALUES  (-1, 'ajouter'),
                (-2, 'modifier'),
                (-3, 'supprimer');

INSERT INTO t_log(table_origine, identite_utilisateur, date, id_log_type) 
	VALUES ('t_mesure', 'antoine martin', '2019-02-01', -1),
	       ('t_mesure', 'matthieu givaudan', '2019-02-01', -1);
               

