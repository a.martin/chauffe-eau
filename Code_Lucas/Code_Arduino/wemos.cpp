//@Lucas Rodriguez

#include "Arduino.h"
#include "wemos.h"

float heureBoucle;
float l_min; //Calcul du centilitre par minute
volatile int pulseCount;
//Nous avons dû utiliser des variables globales et une fonction, car lorsque nous avons voulu incrémenter le flow
//en tant que méthodes et l'utiliser dans notre fonction "attachInterrupt" cela provoquait une erreure que l'on a
//fonc régler de cette façon

wemos::wemos() 
{
  pinMode(4,INPUT);
  digitalWrite(4,HIGH);
  attachInterrupt(digitalPinToInterrupt(4), flow, FALLING);
  // Configuration mode FALLING c'est à dire passage de l'état HIGH à l'état LOW
  heureBoucle = millis();
}

float wemos::temp()
{ 
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0/Vo-1.0);
  logR2 = log(R2);
  T = (1.0 / (c1+c2*logR2 +c3*logR2*logR2*logR2));
  T2 = T - 273.15;
  return T2;
}

float wemos::debit()
{
  pulseCount--; //En raison de l'appel de flow, décrémentation du pulseCount
  flow(); //Pour réincrémenter le flow
  return l_min;
}

void flow()
{
  pulseCount++; //Incrémente le pulseCount
  float frec;
  long dureeBoucle = millis() - heureBoucle;
  if(dureeBoucle > 999)
  {
     frec = (float)pulseCount / (float)dureeBoucle * 1000;
     //Calcul de l_min, qui est égale au nombre de pulsation, divisé par la durée de la boucle fois 1000 pour l'avoir en seconde
     //Diviser par 7.5 pour avoir le débit en L/min car, la fréquence=7.5*débit donc débit=fréquence/7.5
     l_min = (frec/7.5); 
     
     heureBoucle = millis(); //Remise au temps actuel de heureBoucle
     pulseCount = 0; // Remise à 0 du pulseCount, pour pouvoir recommencer
  }
}
