//@Lucas Rodriguez

#ifndef wemos_h
#define wemos_h
#include "Arduino.h"

#define ThermistorPin A0  //Nom du pin
#define R1 10000 //Résistance utilisé pour le 
#define c1 0.001129148 //c1, c2 et c3 sont des constantes permettant le calcul
#define c2 0.000234125 //de la température
#define c3 0.0000000876741

class wemos
{
  public:
  wemos(); //Constructeur
  float temp(); //Fonction pour utiliser le capteur de température
  float debit(); //Fonction pour utiliser le capteur de débit
    
  private:
  //Variable nécessaire dans le déroulement du programme :
  float Vo;
  float logR2;
  float R2;
  float T;
  float T2;
  float Tc;
};
void flow();
#endif
