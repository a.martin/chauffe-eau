//@Vincent PEIRANO et Lucas RODRIGUEZ

#ifndef socket_h
#define socket_h
#include "Arduino.h"

class Socket
{
  public:
  Socket();
  ~Socket();
  char Envoyer(String);
  int methode();
  
  private:
  char ssid[]; //SSID of your Wi-Fi router
  char pass[]; //Password of your Wi-Fi router
  byte server[];
};
#endif 
