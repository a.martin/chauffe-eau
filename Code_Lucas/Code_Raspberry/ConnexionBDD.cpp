#include "ConnexionBDD.h"

using namespace std;

PGresult *res;
PGconn *conn;

ConnexionBDD::ConnexionBDD()
{
	this->conn = PQconnectdb("user=pi password=chauffeeau dbname=ChauffeEau"); //connexion a la base de données
	
	if (PQstatus(conn) == CONNECTION_BAD) {	//vérification de la connexion

        cout << PQerrorMessage(conn) << endl;
        PQfinish(conn);	//si il y a une erreur de connexion, on ferme la connexion
        exit(1);
    }
}

ConnexionBDD::~ConnexionBDD(){}

void ConnexionBDD::do_exit(PGconn *conn, PGresult *res) 
{ 
    fprintf(stderr, "%s\n", PQerrorMessage(conn));    

    PQclear(res);	//clear la requete
    PQfinish(conn); //ferme la connexion
    
    exit(1);
}

int ConnexionBDD::ExecuteUpdate(const char* ctmp)
{
	res = PQexec(conn, ctmp);	//execution de la requete, res prend le resultat de la requete
        
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {	//si la requete ne s'effectue pas, on sort de la methode
		do_exit(conn, res); 
	}
	
	PQclear(res);	//clear la requete
	
	return 0;
}

int ConnexionBDD::ExecuteInsert(const char* ctmp)
{
	res = PQexec(conn, ctmp);	//execution de la requete, res prend le resultat de la requete
        
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {	//si la requete ne s'effectue pas, on sort de la methode
		do_exit(conn, res); 
	}		
	
	PQclear(res);	//clear la requete
	
	return 0;
}

int ConnexionBDD::ExecuteSelect(const char* ctmp, float *array[200])
{
	res = PQexec(conn, ctmp);	//execution de la requete, res prend le resultat de la requete
	
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {	//si la requete ne s'effectue pas, on sort de la methode
		do_exit(conn, res); 
	}
	
	int rows = PQntuples(res);	//clear la requete
    
    for(int i=0; i<rows; i++) {					//permet de stocké dans un tableau le resultat de la commande select
        *array[i] = (int)PQgetvalue(res, i, 0);
    }
    
    return 0;
}

    
    
