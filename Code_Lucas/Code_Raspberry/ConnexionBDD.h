#ifndef CONNEXIONBDD_H
#define CONNEXIONBDD_H
#define lqt LUCASLAREINEDESTCHOINS


#include <iostream>
#include <string>
#include <postgresql/libpq-fe.h>
#define DBNAME "ChauffeEau"
#define USER "pi"
#define PSW "chauffeeau"

using namespace std;

class ConnexionBDD
{
	public:
	PGconn *conn;
	PGresult *res;
	ConnexionBDD();
	~ConnexionBDD();
	void do_exit(PGconn *conn, PGresult *res);
	int ExecuteUpdate(const char*);
	int ExecuteInsert(const char*);
	int ExecuteSelect(const char*, float *array[200]);
};

#endif

