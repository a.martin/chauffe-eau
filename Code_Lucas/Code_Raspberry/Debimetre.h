#ifndef DEBIMETRE_H
#define DEBIMETRE_H

#include <iostream>
#include <wiringPi.h>
#include <thread>
#include <string>
#include <sstream>
#include "Date.h"
#include "ConnexionBDD.h"

using namespace std;

class Debimetre
{
	private:
	float leDernierDebitPrincipal;
	float leDernierDebitSecondaire;
	
	public:
	int lePin;
	string idBDD;
	long heureBoucle;
	long heureInsertion;
	Debimetre(int lePin, string idBDD);
	~Debimetre();	
	float DebitPrincipal();
	float DebitSecondaire();
	int Flow();
	bool Demarre();
	int EcrireDansBDDPrincipal(float, string);
	int EcrireDansBDDSecondaire(float, string);
};
void counterC_11();
void counterC_912();

#endif

