#include "Debimetre.h"

using namespace std;

string convertDouble(double value) {
  std::ostringstream o;
  if (!(o << value))
    return "";
  return o.str();
}

int counter_11;
int counter_12;
string idBDD;

extern "C" int wiringPiISR (int pin, int edgeType, void (* function) (void));
#define INT_EDGE_FALLING 1

Debimetre::Debimetre(int lePin, string idBDD)
{
	this->idBDD = idBDD;
	this->lePin = lePin;
	pinMode(lePin, INPUT);
	digitalWrite(lePin, HIGH);
}

Debimetre::~Debimetre(){}

void counterC_11(){
	counter_11++;
}
void counterC_12(){
	counter_12++;
}

int Debimetre::EcrireDansBDDPrincipal(float leDernierDebitPrincipal, string date){
	ConnexionBDD *base = new ConnexionBDD();	//instanciation de l'objet base permettant l'échange avec la base de données
	Date *date2 = new Date();  				    //instanciation de l'objet date
	string sDate;
	date2->DateNow(&sDate) ;
	string tmp = "INSERT INTO t_mesure(valeur_mesure, heure_mesure, id_capteur) VALUES ("+ convertDouble(leDernierDebitPrincipal) + ", '"+sDate+"', '"+idBDD+"')";
	const char *ctmp = tmp.c_str();
	base->ExecuteInsert(ctmp);
	cout<<tmp<<endl;
	return 0;
}

int Debimetre::EcrireDansBDDSecondaire(float leDernierDebitSecondaire, string date){
	ConnexionBDD *base = new ConnexionBDD();	//instanciation de l'objet base permettant l'échange avec la base de données
	Date *date2 = new Date();  				    //instanciation de l'objet date
	string sDate;
	date2->DateNow(&sDate) ;
	string tmp = "INSERT INTO t_mesure(valeur_mesure, heure_mesure, id_capteur) VALUES ("+ convertDouble(leDernierDebitSecondaire) + ", '"+sDate+"', '"+idBDD+"')";
	const char *ctmp = tmp.c_str();
	base->ExecuteInsert(ctmp);
	cout<<tmp<<endl;
	return 0;
}

bool Debimetre::Demarre(){
	if (this->lePin == 11)
		wiringPiISR(this->lePin, INT_EDGE_FALLING, counterC_11);
	if (this->lePin == 12)
		wiringPiISR(this->lePin, INT_EDGE_FALLING, counterC_12);
		
	heureBoucle = millis();
	heureInsertion = millis();
	
	while(1)
	{
		this->Flow();
		delay(1000);
	}
	return true;
}

int  Debimetre::Flow()
{
	float frec;
	float frec2;
	string date;
	long dureeBoucle = millis()-heureBoucle;
	long dureeInsertion = millis()-heureInsertion;
	
	if(dureeBoucle > 999){
		if (this->lePin == 11){
			frec = (float)counter_11 / (float)dureeBoucle*1000;
			cout<<"Counter pin 11: "<<counter_11<<endl;
			float l_min = (frec/7.5);
			cout << l_min << "L/min" << endl;
			this->leDernierDebitPrincipal = l_min;
			if(dureeInsertion > 29999){
				this->EcrireDansBDDPrincipal(leDernierDebitPrincipal, date);
				heureInsertion = millis();
			}
		}
		
		if (this->lePin == 12){
			frec2 = (float)counter_12 / (float)dureeBoucle*1000;
			cout<<"Counter pin 12: "<<counter_12<<endl;
			float l_min2 = (frec2/7.5);
			cout<< l_min2 << "L/min" <<endl;
			this->leDernierDebitSecondaire = l_min2;
			if(dureeInsertion > 29999){
				this->EcrireDansBDDPrincipal(leDernierDebitSecondaire, date);
				heureInsertion = millis();
			}
		}
		
		heureBoucle = millis();
		
		
		if (this->lePin == 11)
			counter_11 = 0;
			
		if (this->lePin == 12)
			counter_12 = 0;
	}
	return 0;
}

float Debimetre::DebitPrincipal(){
	return leDernierDebitPrincipal;	
}

float Debimetre::DebitSecondaire(){
	return leDernierDebitSecondaire;
}
