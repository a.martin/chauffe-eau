#include <iostream>
#include <fstream>
#include <string>
#include <wiringPi.h>
#include <thread>
#include "/home/pi/Downloads/curl-7.64.1/include/curl/curl.h"
#include "ConnexionBDD.h"
#include "Date.h"
#include "Debimetre.h"
#include "Param.h"

int main()
{	
	wiringPiSetup();				//initialise wiringPi pour utiliser les pins
	wiringPiSetupSys();				//utilise l'interface /sys/class/gpio plutot qu'un acces matériel direct
	wiringPiSetupGpio();			//permet d'utiliser les noms GPIO
	wiringPiSetupPhys();			//permet d'utiliser les numéros physique des pins
	
	debimetrePrincipal =  new Debimetre(11, "-2");
	debimetreSecondaire = new Debimetre(12, "-17");
	std::thread first (DemarrePrincipal);
	std::thread secondaire (DemarreSecondaire);
	char buffer[10] ;
	cin >> buffer ;
	return 0;
};
