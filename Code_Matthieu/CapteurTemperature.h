#ifndef CAPTEUR_TEMPERATURE_H
#define CAPTEUR_TEMPERATURE_H

#include "ChauffeEau.h"

using namespace std;

class CapteurTemperature
{
	public:
		std::string nomCapteur;
		int numeroCapteur;
		CapteurTemperature(std::string nomCapteur, int numeroCapteur);
		~CapteurTemperature();
		int LireTemp(float *celc);
};

#endif
