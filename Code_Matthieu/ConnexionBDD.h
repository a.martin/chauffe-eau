#ifndef CONNEXIONBDD_H
#define CONNEXIONBDD_H

#include <postgresql/libpq-fe.h>
#include <string>
#include <iostream>
#define DBNAME "ChauffeEau"
#define USER "pi"
#define PSW "chauffeeau"

using namespace std;

class ConnexionBDD
{
	public:
	PGconn *conn;
	PGresult *res;
	ConnexionBDD();
	~ConnexionBDD();
	void do_exit(PGconn *conn, PGresult *res);
	int ExecuteUpdate(const char*);
	int ExecuteInsert(const char*);
	int ExecuteSelect(const char*, float*);
};

#endif
