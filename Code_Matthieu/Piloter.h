#ifndef PILOTER_H
#define PILOTER_H

//#include "ChauffeEau.h"
#include <iostream>
#include <wiringPi.h>
#include "ConnexionBDD.h"

using namespace std;

class Piloter
{
	public:
	int pin;
	Piloter(int pin);
	~Piloter();
	int Allumer();
	int Eteindre();
};

#endif
