#include "MDate.h"

using namespace std;

MDate::MDate(){}

MDate::~MDate(){}

int MDate::MDateNow(string *datenow)
{
	time_t now = time(0);
	tm *ltm = localtime(&now);
   
    string year = to_string(1900 + ltm->tm_year); 	//mise en place de l'annee
    string month = to_string(1 + ltm->tm_mon);		//mise en place du mois
    string day = to_string(ltm->tm_mday);			//mise en place du jour
    string hour = to_string(ltm->tm_hour);			//mise en place de l'heure
    string min = to_string(ltm->tm_min);			//mise en place des minutes
    string sec = to_string(ltm->tm_sec);			//mise en place des secondes
    
    *datenow = year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec; //mise en forme de la date pour correspondre a la base de données
    
    return 0;
}

int MDate::MHourNow(int *hournow)
{
	time_t now = time(0);
	tm *ltm = localtime(&now);
	
	int hour = ltm->tm_hour;			//mise en place de l'heure
	*hournow = hour;
	return 0;
}
