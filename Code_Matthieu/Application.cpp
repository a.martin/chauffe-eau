#include "Application.h"

using namespace std;

Application::Application(){}

Application::~Application(){}

int Application::Demarre()
{
	wiringPiSetup();				//initialise wiringPi pour utiliser les pins
	wiringPiSetupSys();				//utilise l'interface /sys/class/gpio plutot qu'un acces matériel direct
	wiringPiSetupGpio();			//permet d'utiliser les noms GPIO
	wiringPiSetupPhys();			//permet d'utiliser les numéros physique des pins
	bool pompeActive = false;
	bool resistanceActive = false;
	Pompe *pompe = new Pompe(8);					//instanciation de l'objet pompe
	Resistance *resistance = new Resistance(10);	//instanciation de l'objet resistance
	MDate *date = new MDate(); 						//instanciation de l'objet date
	Accumulateur *accumulateur = new Accumulateur();//instanciation de l'objet accumulateur
	LienBDD *lienbdd = new LienBDD();				//instanciation de l'objet lienbdd

	resistance->Eteindre();		//on initialise la resistance comme éteinte au démarrage
	pompe->Eteindre();			//on initialise la pompe comme éteinte au démarrage
		
	while (1){
		accumulateur->EnregistrementTemperature();		//enregistrement des valeurs thermiques des thermometres de l'accumulateur
		
		/*std::thread first (DemarreDebimetre);
		debimetre->DemarreDebimetre();
		first.join();*/
		
		float temperature_moyenne_accumulateur;
		lienbdd->TemperatureMoyenneAccumulateur(&temperature_moyenne_accumulateur);	//récupération de la température moyenne a l'intérieur de l'accumulateur
		float pourcentage_nuage;
		lienbdd->PourcentageNuage(&pourcentage_nuage); //récupération de la nébulosité du lendemain
		float dernier_capteur;
		float temperature_capteur_solaire;
	
		lienbdd->TemperatureCapteurSolaire(&temperature_capteur_solaire); //récupération de la température du capteur de température a la sortie du capteur solaire
		
		lienbdd->TemperatureDernierCapteur(&dernier_capteur); //récupération de la température du dernier capteur dans l'accumulateur
		
		if(temperature_capteur_solaire > dernier_capteur + DIFF_TEMP && pompeActive == false) //Si la température du thermometre du capteur solaire est supérieur et 4°C de plus que le dernier thermometre accumulateur
		{																					  // et que la pompe n'est pas activée.
			pompe->Allumer();	//allumage de la pompe
			pompeActive = true;
		}
		else if (temperature_capteur_solaire <= dernier_capteur + DIFF_TEMP && pompeActive == true)//Si la temperature du capteur solaire est inférieur ou egale et 4°C de moins que le dernier thermometre accumulateur
		{																						   // et que la pompe est allumée
			pompe->Eteindre();	//On éteint la pompe
			pompeActive = false;
		}	
					
		int heure;
		date->MHourNow(&heure);	
		
		if((heure >= 22 || heure < 6) && pourcentage_nuage >= POURCENTAGE_NUAGE && resistanceActive == false && pompeActive == false)
		{ //Si l'heure est comprise entre 22 et 6, et que la nébulosité est supérieur à 30 et que la pompe et la résistance sont eteintes.
			string dateDebut;
			date->MDateNow(&dateDebut);	//on associe a dateDebut la date et l'heure actuel
			resistance->Allumer(dateDebut); //on allume la resistance et on entre en base la date de démarrage
			resistanceActive = true;
		}		
		else if(((heure < 22 || heure >= 6) || temperature_moyenne_accumulateur >= TEMP_MOYENNE_ACCUMULATEUR) && resistanceActive == true)
		{ //Si l'heure n'est pas comprise entre 22 et 6 , ou que la température moyenne de l'accumulateur est supérieur ou égale a 60)C et que la résistance est active
			string dateFin;
			date->MDateNow(&dateFin);//on associe a dateFin la date et l'heure actuel
			resistance->Eteindre(dateFin);//on éteint la resistance et on entre en base la date d'arret
			resistanceActive = false;
		}	
			
		delay (DELAY);
	}
    return 0;
}

