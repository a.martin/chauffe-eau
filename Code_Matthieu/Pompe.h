#ifndef POMPE_H
#define POMPE_H

#include "ChauffeEau.h"

using namespace std;

class Pompe:public Piloter
{
	public:
		Pompe(int pin);
		virtual ~Pompe();
};

#endif
