#include "Resistance.h"

Resistance::Resistance(int pin):Piloter(pin)
{
	base = new ConnexionBDD();
}

Resistance::~Resistance(){}

int Resistance::Allumer(string dateDebut)
{	
	digitalWrite(pin,0);
	
	string tmp = "UPDATE t_consommationresistance SET heure_fin_conso = '"+dateDebut+"' WHERE heure_fin_conso is NULL RETURNING id_conso;"; //commande sql permettant la mise a jour du champ heure_fin_conso de la table t_consommationresistance
	const char *ctm2 = tmp.c_str();	//converison de chaine de caratère en constante
	base -> ExecuteUpdate(ctm2);	//execution de la commande sql
	
	tmp = "INSERT INTO t_consommationresistance (heure_debut_conso) VALUES ('"+dateDebut+"')"; //commande sql permettant l'insertion du champ heure_debut_conso de la table t_consommationresistance
	const char *ctmp3 = tmp.c_str(); //converison de chaine de caratère en constante
	base-> ExecuteInsert(ctmp3);	 //execution de la commande sql
	
	return 0;
}

int Resistance::Eteindre(string dateFin)
{	
	digitalWrite(pin,1);
	string tmp = "UPDATE t_consommationresistance SET heure_fin_conso = '"+dateFin+"' WHERE heure_fin_conso is NULL RETURNING id_conso"; //commande sql permettant la mise a jour du champ heure_fin_conso de la table t_consommationresistance
	const char *ctmp = tmp.c_str();	//conversion de chaine de caratère en constante
	base -> ExecuteUpdate(ctmp);	//execution de la commande sql
	
	return 0;
}

int Resistance::Eteindre()
{
	return Piloter::Eteindre();
}
