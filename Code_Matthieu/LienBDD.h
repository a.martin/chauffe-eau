#ifndef LIENBDD_H
#define LIENBDD_H

#include <iostream>
#include "ConnexionBDD.h"

using namespace std;

class LienBDD
{
	public:
	ConnexionBDD *base;
	LienBDD();
	~LienBDD();
	int TemperatureMoyenneAccumulateur(float*);
	int TemperatureCapteurSolaire(float*);
	int PourcentageNuage(float*);
};

#endif


