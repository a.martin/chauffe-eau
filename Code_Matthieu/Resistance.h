#ifndef RESISTANCE_H
#define RESISTANCE_H

#include "Piloter.h"

using namespace std;

class Resistance : public Piloter
{
	public:
		ConnexionBDD *base;
		Resistance(int pin);
		virtual ~Resistance();
		int Allumer(string dateDebut);
		int Eteindre(string dateFin);
		int Eteindre();
};

#endif
