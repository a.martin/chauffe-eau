#ifndef CHAUFFEEAU_H
#define CHAUFFEEAU_H

#include <iostream>
#include <fstream>
#include <string>
#include <wiringPi.h>
#include <postgresql/libpq-fe.h>
#include "/home/pi/Downloads/curl-7.64.1/include/curl/curl.h"
#include "Application.h"
#include "CapteurTemperature.h"
#include "Piloter.h"
#include "Pompe.h"
#include "Resistance.h"
#include "ConnexionBDD.h"
#include "Accumulateur.h"
#include "MDate.h"
#include "LienBDD.h"

#define TEMP_MOYENNE_ACCUMULATEUR 60
#define POURCENTAGE_NUAGE 30
#define DELAY 30000
#define CAPTEUR_TEMPERATURE_1 "/sys/bus/w1/devices/28-031197799cb0/w1_slave"
#define CAPTEUR_TEMPERATURE_2 "/sys/bus/w1/devices/28-03119779506b/w1_slave"
#define CAPTEUR_TEMPERATURE_3 "/sys/bus/w1/devices/28-031197797d08/w1_slave"
#define CAPTEUR_TEMPERATURE_4 "/sys/bus/w1/devices/28-03119779574c/w1_slave"
#define CAPTEUR_TEMPERATURE_5 "/sys/bus/w1/devices/28-031197797cdd/w1_slave"
#define CAPTEUR_TEMPERATURE_6 "/sys/bus/w1/devices/28-0218924575ea/w1_slave"
#define CAPTEUR_TEMPERATURE_7 "/sys/bus/w1/devices/28-0214924586bc/w1_slave"
#define CAPTEUR_TEMPERATURE_8 "/sys/bus/w1/devices/28-021892458a30/w1_slave"
#define CAPTEUR_TEMPERATURE_9 "/sys/bus/w1/devices/28-021892459846/w1_slave"

#endif
