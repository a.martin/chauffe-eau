#include "LienBDD.h"

using namespace std;

LienBDD::LienBDD()
{
	base = new ConnexionBDD();	//instanciation de l'objet base permettant l'échange avec la base de données
}

LienBDD::~LienBDD(){}

int LienBDD::TemperatureMoyenneAccumulateur(float* TempMoyenneAccumulateur)
{
	string tmp = "SELECT AVG(valeur_mesure) FROM (SELECT valeur_mesure FROM t_mesure WHERE id_capteur BETWEEN -14 AND -6 ORDER BY heure_mesure DESC LIMIT 9) tmp"; //commande sql permettant la récupération des 9 dernières valeur thermiques des capteurs -6  à -14
	const char *ctmp = tmp.c_str();	//converison de chaine de caratère en constante 
	base -> ExecuteSelect(ctmp, TempMoyenneAccumulateur);	//execution de la commande sql
	return 0;
}

int LienBDD::TemperatureCapteurSolaire(float* tempCapteurSolaire)
{
	string tmp = "SELECT valeur_mesure FROM t_mesure WHERE id_capteur=-4 ORDER BY heure_mesure DESC LIMIT 1"; //commande sql permettant la récupération température à la sortie du capteur solaire
	const char *ctmp = tmp.c_str();	//converison de chaine de caratère en constante 
	base -> ExecuteSelect(ctmp, tempCapteurSolaire);	//execution de la commande sql
	
	return 0;
}

int LienBDD::PourcentageNuage(float* pourcentage_nuage)
{
	string tmp = "SELECT pourcentage_nuage FROM t_previsionmeteo_heure ORDER BY dateheure_fin DESC LIMIT 1"; //commande sql permettant la récupération de du pourcentage de nuage
	const char *ctmp = tmp.c_str();	//conversison de chaine de caratère en constante
	base -> ExecuteSelect(ctmp, pourcentage_nuage);	//execution de la commande sql	
	return 0;
}
