#include "Piloter.h"

using namespace std;

Piloter::Piloter(int pin)
{
	this->pin = pin;
	pinMode(pin,OUTPUT);
}

Piloter::~Piloter(){};

int Piloter::Allumer()
{	
	digitalWrite(pin,0);		//passage du pin a l'etat 0
	return 0;
}

int Piloter::Eteindre()
{
	digitalWrite(pin,1);		//passage du pin a l'etat 0
	return 0;
}
