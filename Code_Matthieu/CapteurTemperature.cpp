#include "CapteurTemperature.h"

using namespace std;

CapteurTemperature::CapteurTemperature(std::string nomCapteur, int numeroCapteur)
{
	this->nomCapteur = nomCapteur;
	this->numeroCapteur = numeroCapteur;
};

CapteurTemperature::~CapteurTemperature(){};

int CapteurTemperature::LireTemp(float *celc)
{
	std::ifstream infile(nomCapteur); 									//permet de lire le fichier
	std::string line;
	std::getline(infile,line);											//passage de la première ligne (pas importante)
	std::getline(infile,line);											//traitement de la deuxième ligne qui nous intéresse
	if (line  == "")													//si les lignes sont vide, le capteur n'est pas connecté
	{
		cout << "fichier vide" << endl;									//on le signal a l'utilisateur
		return 1;
	}
	if (line.find("t=") == std::string::npos)							//si il n'y a aucun contenu apres le t= dans le fichier
	{
		cout << "aucune température" << endl;							//on signal qu'il n'y a aucune température relevé
		return 2;
	}
	std::string temp = line.substr(line.find("t=") + 2);				//récupération du texte apres le t=
	*celc = std::stof(temp, 0)/1000;									//conversion et divise la 1000 la valeur obtenu pour l'avoir en °C
	//std::cout<< numeroCapteur << ")  " << *celc << "°C |" << std::endl;	//affichage console de la température obtenue
	return 0;
}
