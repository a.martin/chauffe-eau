#include "Accumulateur.h"

using namespace std;

Accumulateur::Accumulateur(){}

Accumulateur::~Accumulateur(){}

int Accumulateur::EnregistrementTemperature()
{
	CapteurTemperature *capteur[9];
			capteur[0] = new CapteurTemperature(CAPTEUR_TEMPERATURE_1, 1); //instanciation de tous les thermometres numériques dans un tableau
			capteur[1] = new CapteurTemperature(CAPTEUR_TEMPERATURE_2, 2);
			capteur[2] = new CapteurTemperature(CAPTEUR_TEMPERATURE_3, 3);
			capteur[3] = new CapteurTemperature(CAPTEUR_TEMPERATURE_4, 4);
			capteur[4] = new CapteurTemperature(CAPTEUR_TEMPERATURE_5, 5);
			capteur[5] = new CapteurTemperature(CAPTEUR_TEMPERATURE_6, 6);
			capteur[6] = new CapteurTemperature(CAPTEUR_TEMPERATURE_7, 7);
			capteur[7] = new CapteurTemperature(CAPTEUR_TEMPERATURE_8, 8);
			capteur[8] = new CapteurTemperature(CAPTEUR_TEMPERATURE_9, 9);
			
	for ( int i = 0; i <= 8 ; i++)		//boucle qui s'executera 9 fois
	{
		int idCapteur = i+1;
		float temperature;
		int resultatLecture;
		resultatLecture = capteur[i]->LireTemp(&temperature);		//Lire la température d'un capteur
		if(resultatLecture == 0){
			cout << temperature << endl; //affichage de la température dans la console
			
			char save[150];
			sprintf(save,"http://localhost/chauffe-eau-master/src/enregistrementTemperatureNumerique.php?idCapteur=Temperature%dDansAccumulateur&temperature=%f", idCapteur,temperature); //stockage de l'url avec les informations de température et d'id dans la chaine "save"
			CURL *curl;
			CURLcode res;
			curl = curl_easy_init();
		
			if(curl) 
			{
				curl_easy_setopt(curl, CURLOPT_URL, save);			//connexion a l'url "save" pour utiliser la méthode d'enregistrement						
				curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L); //save est redirigée, on dit a libcurl de suivre la redirection
			}
			
			res = curl_easy_perform(curl);	//Execute la requete, res contient la valeur de retour
			
			if(res != CURLE_OK) //Verification d'erreur
			{	
				fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));		  
				curl_easy_cleanup(curl); 		//ferme toutes les connexions
			}					
			delete capteur[i]; //suppression de chaque objet CapteurTemperature
		}
	}
	return 0;
}
