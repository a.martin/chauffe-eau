#include "ChauffeEau.h"

using namespace std;

int main()
{	
	wiringPiSetup();				//initialise wiringPi pour utiliser les pins
	wiringPiSetupSys();				//utilise l'interface /sys/class/gpio plutot qu'un acces matériel direct
	wiringPiSetupGpio();			//permet d'utiliser les noms GPIO
	wiringPiSetupPhys();			//permet d'utiliser les numéros physique des pins
	int pompeActive = 0;
	int resistanceActive = 0;
	float temperature;
	int resultatLecture;
	char choix;
	Pompe *pompe = new Pompe(8);					//instanciation de l'objet pompe
	Resistance *resistance = new Resistance(10);	//instanciation de l'objet resistance
	Date *date = new Date(); 						//instanciation de l'objet date
	Accumulateur *accumulateur = new Accumulateur();
	//Debimetre *debimetre =  new Debimetre(11);
	resistance->Eteindre();
	pompe->Eteindre();
		
	while (1){																//boucle infinie
		
		cout << "t pour afficher les températures" << endl;
		cout << "p pour contrôler la pompe" << endl;
		cout << "r pour contrôler la résistance" << endl;
		//cout << "d pour afficher le débit réseau primaire" << endl;
		cin >> choix ;
		
		switch(choix) {														//switch permettant le choix d'action
		case 't' :
		case 'T' :
			accumulateur->EnregistrementTemperature();
			break;
		case 'p' : 
		case 'P' :
			while (1){
				cout << "0 : Eteindre" << endl;
				cout << "1 : Allumer" << endl;
				cout << "Autre : Quitter" << endl;
				cin >> pompeActive;
				if(pompeActive == 0){			//si l'utilisateur entre 0, la pompe s'éteint
					pompe->Eteindre();
				} else if ( pompeActive == 1) { //si l'utilisateur entre 1, la pompe s'active
					pompe->Allumer();
				} else {						//si l'utilisateur quitte, la pompe s'éteint
					pompe->Eteindre();
					break;
				}
			}
			break;
		case 'r' : 
		case 'R' :
			while (1){
				cout << "0 : Eteindre" << endl;
				cout << "1 : Allumer" << endl;
				cout << "Autre : Quitter" << endl;
				cin >> resistanceActive;
				if(resistanceActive == 0){				//si l'utilisateur entre 0, la résistance s'éteint
					string dateFin;
					date->DateNow(&dateFin);			//dateFin prend la valeur de la date et l'heure actuelle
					resistance->Eteindre(dateFin);		//la resistance s'éteint en enregistrant la date de fin						
				} else if ( resistanceActive == 1) {	//si l'utilisateur entre 0, la résistance s'éteint
					string dateDebut;
					date->DateNow(&dateDebut);			//dateDebut prend la valeur de la date et l'heure actuelle
					resistance->Allumer(dateDebut);		//la resistance s'éteint en enregistrant la date de debut												
				} else {								//si l'utilisateur quitte, la résistance s'éteint	
					string dateFin;
					date->DateNow(&dateFin);			//dateFin prend la valeur de la date et l'heure actuelle
					resistance->Eteindre(dateFin);		//la resistance s'éteint en enregistrant la date de fin	
					break;
				}
			}
			break;
		/*case 'd':
		case 'D':
			while(1)
			{
				debimetre->Flow();
			}
		*/default :										// le fault du switch eteint tout, en cas de problème
			string dateFin;
			date->DateNow(&dateFin);
			pompe->Eteindre();
			resistance->Eteindre(dateFin);
			break;
		}
	}
	
    return 0;
}
