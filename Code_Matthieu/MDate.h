#ifndef MDATE_H
#define MDATE_H

#include "ChauffeEau.h"

using namespace std;

class MDate
{
	public:
	MDate();
	~MDate();
	int MDateNow(string *datenow);
	int MHourNow(int *hournow);
};

#endif
